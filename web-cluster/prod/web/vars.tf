variable "conf" {
  default = {
    "region" : "eu-west-1",
    "ami" : "ami-0aef57767f5404a3c"
  }
}

variable "server_port" {
  default = 80
}

data "aws_vpc" "default" {
  default = true
}

data "aws_subnet_ids" "default_subnets_ids" {
  vpc_id = data.aws_vpc.default.id
}

variable "env" {
  default = "prod"
}
